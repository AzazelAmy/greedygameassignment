*** Settings ***
Documentation    Suite description
Resource            ../keywords/common.robot
Resource            ../keywords/top_charts.robot
Suite Setup         Open Browser and Navigate to PlayStore Top Charts page
Suite Teardown      Close Opened Browser

*** Test Cases ***
TEST CASE #01: Fetch Application Details from PlayStore
    Fetch App Name and App Details from PlayStore
