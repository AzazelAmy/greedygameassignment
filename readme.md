# GreedyGame Assignment

## Coded Using
* IDE - PyCharm CE
* Framework - RobotFramework
* External Libraries - SeleniumLibrary, DateTimeTZ

## Info about the files

* keywords folder contains the code that is used as steps for the assignment
* locators_and_variables folder contains code for some static locator and variable used in project
* result folder contains the xls sheet listing the app name, total ratings, last update date, no. of days before last update and score of the app
* testcases folder contains the code used to run the testcase with suite setup and teardown
* report.html contains in-detailed steps of the test-run performed
