*** Variables ***
${rating_locator}               css:.EymY4b > span:nth-of-type(2)
${last_update_locator#1}        css:.IxB2fe > div:nth-of-type(1) > .htlgb > .IQ1z0d > .htlgb
${last_update_locator#2}        css:.IxB2fe > div:nth-of-type(2) > .htlgb > .IQ1z0d > .htlgb

${regexp}                       (January|February|March|April|May|June|July|August|September|October|November|December)\\s+(\\d{1,2})+\\,+\\s+(\\d{4})