*** Settings ***
Documentation    Suite description
Library         SeleniumLibrary

*** Keywords ***
Open Browser and Navigate to PlayStore Top Charts page
    open browser  https://play.google.com/store/apps/top  chrome
    maximize browser window

Close Opened Browser
    delete all cookies
    close browser