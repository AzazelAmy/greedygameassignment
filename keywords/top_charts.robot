*** Settings ***
Documentation    Suite description
Library         String
Library         ExcelRobot
Library         DateTimeTZ
Library         DateTime
Resource        ../locators_and_variables/app_details.robot

*** Keywords ***
Fetch App Name and App Details from PlayStore
    set test variable  ${count}  ${0}
    FOR  ${index}  IN RANGE  1  7
    ${column_locator}=  catenate  SEPARATOR=  /html[1]/body[1]/div[1]/div[4]/c-wiz[1]/div[1]/c-wiz[1]/div[1]/div[1]/c-wiz[1]/c-wiz[  ${index}  ]/c-wiz[1]/div[1]/div[2]
    set test variable  ${column_locator}
    Fetch App Name for a Single Row
    END

Fetch App Name for a Single Row
    FOR  ${index}  IN RANGE  1  7
    ${row_locator}=  catenate  SEPARATOR=  /div[  ${index}  ]/c-wiz[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]/div[1]
    ${app_locator}=  catenate  SEPARATOR=  xpath:  ${column_locator}  ${row_locator}
    ${app_name}=  get text  ${app_locator}
    set test variable  ${app_name}                                                                                    #fetched app name from the top-charts page
    click element  ${app_locator}
    wait until page does not contain element  css:trans-layer                                                           #wait until the loading spinner animation is over
    wait until page contains  ${app_name}                                                                               #ensures that page content is displayed
    ${app_rating}=  get text  ${rating_locator}
    set test variable  ${app_rating}
    Fetch Last Updated Date from App Details Section
    go back
    wait until location contains  top
    Write App Details in Excel Sheet
    END

Write App Details in Excel Sheet
    open excel to write  result/app_score.xls
    ${count}=  set variable  ${count+1}
    set test variable  ${count}
    Write App Name to a Particular Column
    Write App Ratings to a Particular Column
    Write App Last Updated Date to a Particular Column
    Write No. of Days Since Last Update to a Particular Column
    Calculate Score of the Application and Write it to the Adjacent Rows
    Save Excel

Write App Name to a Particular Column
    ${name_column}=  convert to integer  0
    write to cell  App_Score  ${name_column}  ${count}  ${app_name}  TEXT

Write App Ratings to a Particular Column
    ${rating_column}=  convert to integer  1
    write to cell  App_Score  ${rating_column}  ${count}  ${app_rating}  TEXT

Write App Last Updated Date to a Particular Column
    ${date_column}=  convert to integer  2
    write to cell  App_Score  ${date_column}  ${count}  ${app_last_update}  TEXT

Write No. of Days Since Last Update to a Particular Column
    ${days_column}=  convert to integer  3
    write to cell  App_Score  ${days_column}  ${count}  ${days_since_last_update}  TEXT

Calculate Score of the Application and Write it to the Adjacent Rows
    ${app_rating}=  remove string  ${app_rating}  ,
    ${app_rating}=  convert to integer  ${app_rating}
    ${formatted_days}=  convert to integer  ${formatted_days}
    ${score}=  evaluate  ${app_rating}/${formatted_days}
    ${score_column}=  convert to integer  4
    write to cell  App_Score  ${score_column}  ${count}  ${score}  TEXT

Fetch Last Updated Date from App Details Section
    wait until page contains element  ${last_update_locator#1}
    ${app_last_update}=  get text  ${last_update_locator#1}
    ${regexp_match}=  run keyword and return status  should match regexp  ${app_last_update}  ${regexp}                 #last updated date is available under two locators_and_variables
    run keyword if  "${regexp_match}"=="False"  Use Alternative Locator to Fetch Last Updated Date
    set test variable  ${app_last_update}
    ${format_last_update_date}=  convert timestamp format  ${app_last_update}  YYYY-MM-dd
    set test variable  ${format_last_update_date}
    Obtain Current Date and No. of Days since Last Update

Use Alternative Locator to Fetch Last Updated Date
    ${app_last_update}=  get text  ${last_update_locator#2}
    set test variable  ${app_last_update}

Obtain Current Date and No. of Days since Last Update
    ${current_date}=  get current date  time_zone=local  result_format=%Y-%m-%d
    ${days_since_last_update}=  subtract date from date  ${current_date}  ${format_last_update_date}  result_format=verbose
    set test variable  ${days_since_last_update}
    ${formatted_days}=  fetch from left  ${days_since_last_update}  day
    set test variable  ${formatted_days}